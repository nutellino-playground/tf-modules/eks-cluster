# EKS Cluster modules

Original work by : https://github.com/cloudposse/terraform-aws-eks-cluster

## Introduction

The module provisions the following resources:

- EKS cluster of master nodes that can be used together with the [eks-workers](git::https://gitlab.com/nutellino-playground/tf-modules/eks-workers.git?ref=0.0.1) module to create a full-blown cluster
- IAM Role to allow the cluster to access other AWS services
- Security Group which is used by EKS workers to connect to the cluster and kubelets and pods to receive communication from the cluster control plane (see [terraform-aws-eks-workers](https://github.com/cloudposse/terraform-aws-eks-workers))
- The module generates `kubeconfig` configuration to connect to the cluster using `kubectl`

## Usage


Module usage examples:


```hcl
provider "aws" {
  region = "us-west-1"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. map('BusinessUnit','XYZ')"
}

locals {
  # The usage of the specific kubernetes.io/cluster/* resource tags below are required
  # for EKS and Kubernetes to discover and manage networking resources
  # https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html#base-vpc-networking
  tags = "${merge(var.tags, map("kubernetes.io/cluster/eg-testing-cluster", "shared"))}"
}

module "vpc" {
  source     = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc.git?ref=0.0.1"
  namespace  = "eg"
  stage      = "testing"
  name       = "cluster"
  tags       = "${local.tags}"
  cidr_block = "10.0.0.0/16"
}

module "subnets" {
  source              = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc-subnet.git?ref=0.0.1"
  availability_zones  = ["us-west-1a", "us-west-1b", "us-west-1c", "us-west-1d"]
  namespace           = "eg"
  stage               = "testing"
  name                = "cluster"
  tags                = "${local.tags}"
  region              = "us-west-1"
  vpc_id              = "${module.vpc.vpc_id}"
  igw_id              = "${module.vpc.igw_id}"
  cidr_block          = "${module.vpc.vpc_cidr_block}"
  nat_gateway_enabled = "true"
}

module "eks_cluster" {
  source                  = "git::https://gitlab.com/nutellino-playground/tf-modules/eks-cluster.git?ref=0.0.1"
  namespace               = "eg"
  stage                   = "testing"
  name                    = "cluster"
  tags                    = "${var.tags}"
  vpc_id                  = "${module.vpc.vpc_id}"
  subnet_ids              = ["${module.subnets.public_subnet_ids}"]

  # `workers_security_group_count` is needed to prevent `count can't be computed` errors
  workers_security_group_ids   = ["${module.eks_workers.security_group_id}"]
  workers_security_group_count = 1
}

module "eks_workers" {
  source                             = "git::https://gitlab.com/nutellino-playground/tf-modules/eks-workers.git?ref=0.0.1"
  namespace                          = "eg"
  stage                              = "testing"
  name                               = "cluster"
  tags                               = "${var.tags}"
  instance_type                      = "t2.medium"
  vpc_id                             = "${module.vpc.vpc_id}"
  subnet_ids                         = ["${module.subnets.public_subnet_ids}"]
  health_check_type                  = "EC2"
  min_size                           = 1
  max_size                           = 3
  wait_for_capacity_timeout          = "10m"
  associate_public_ip_address        = true
  cluster_name                       = "eg-testing-cluster"
  cluster_endpoint                   = "${module.eks_cluster.eks_cluster_endpoint}"
  cluster_certificate_authority_data = "${module.eks_cluster.eks_cluster_certificate_authority_data}"
  cluster_security_group_id          = "${module.eks_cluster.security_group_id}"

  autoscaling_policies_enabled           = "false"
 
}
```



